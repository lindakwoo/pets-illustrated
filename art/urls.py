from django.urls import path
from .views import get_all_art

urlpatterns = [
    path("", get_all_art, name="get_all_art")
]
