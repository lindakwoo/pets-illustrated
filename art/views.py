from django.shortcuts import render
from .models import Art
# Create your views here.


def get_all_art(request):
    all_art = Art.objects.all()
    context = {"all_art": all_art}
    return render(request, "art/all_art.html", context)
