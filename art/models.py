from django.db import models

# Create your models here.


class Keyword(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Art(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    photo = models.URLField()
    artwork = models.URLField()
    medium = models.CharField(max_length=150)
    keywords = models.ManyToManyField(Keyword)

    def __str__(self):
        return self.name
