from django.contrib import admin
from .models import Art, Keyword

# Register your models here.


@admin.register(Art)
class ArtAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
        'medium',
    ]


@admin.register(Keyword)
class KeywordAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
    ]
